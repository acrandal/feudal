

class Board(object):
    def __init__(self):
        print("Creating a Board")

    @staticmethod
    def create(filename):
        print("Creating board from file: " + filename)
        newBoard = Board()
        return newBoard
