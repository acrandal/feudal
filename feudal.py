#!/usr/bin/env python3
"""
'   Digital version of Avalon Hill's board game Feudal
'       Original Game Copyright 1969
'       Part of their Bookshelf Board Game series
'
'   My version under Creative Commons License:
'   Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
'
'   Contributors:
'       Aaron S. Crandall <acrandal@gmail.com>
'
"""

from board import Board



if __name__ == "__main__":
    print("Starting Feudal Engine")
    newBoard = Board.create("This file?")


    print("Done.")
